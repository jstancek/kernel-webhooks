#!/usr/bin/env python
"""Check for open Mrs with resolved threads, and validates if it was resolved by a valid user."""

import argparse
import os
import sys

from cki_lib import logger
from cki_lib import misc
from gitlab.const import MAINTAINER_ACCESS
from gitlab.const import NO_ACCESS

from webhook import defs
from webhook.graphql import GitlabGraph

LOGGER = logger.get_logger('utils.acks_blocked_check')

OPEN_MRS = """
query OpenMrs($namespace: ID!, $after: String = "") {
  project(fullPath: $namespace) {
    mergeRequests(state: opened, after: $after) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        id
        iid
      }
    }
  }
}
"""

DISCUSSION_RESOLVER_QUERY = """
query DiscussionResolver($namespace: ID!, $mr_iid: String!, $after: String = "") {
  project(fullPath: $namespace) {
    mergeRequest(iid: $mr_iid) {
      discussions(after: $after, first: 15) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          id
          resolved
          notes {
            nodes {
              url
              author {
                username
              }
            }
          }
          resolvedBy {
            username
          }
        }
      }
    }
  }
}
"""

USER_PERMISSION = """
query ProjectMember($namespace: ID!, $username: String!) {
  project(fullPath: $namespace) {
    projectMembers(search: $username) {
      nodes {
        user {
          username
        }
        accessLevel {
          integerValue
        }
      }
    }
  }
}
"""

DISCUSSION_ACTION = """
mutation comment($noteableID: NoteableID!, $discussionID: DiscussionID!, $body: String! ) {
  createNote(input: {body: $body, noteableId: $noteableID, discussionId: $discussionID}) {
    errors
  }
  discussionToggleResolve(input: {id: $discussionID, resolve: false}) {
    errors
  }
}
"""


def improperly_resolved_msg(resolver, author):  # pragma: no cover
    """Return the message to use as a note for commits without bz."""
    msg = f"This discussion has been re-opened as it was improperly resolved by @{resolver}, "
    msg += "discussions must be resolved by a thread participant or a maintainer.\n"
    msg += f"@{author} consider reviewing this discussion and resolving it, if you see fit"
    return msg


def get_mrs_with_discussions(graphql, namespace):
    """Return a list of the MR IDs with their resolved discussions."""
    open_mrs = {}
    mr_list = []
    count = 0

    # Get open MRS
    result = graphql.check_query_results(graphql.client.query(
        OPEN_MRS,
        variable_values={'namespace': namespace},
        paged_key='project/mergeRequests',
    ), check_keys={'project'})
    open_mrs = {m['iid']: m["id"]
                for m in misc.get_nested_key(result, 'project/mergeRequests/nodes')}

    # Filter by MRs with resolved discussions
    for mr_iid, mr_id in open_mrs.items():
        discussion_list = []

        result = graphql.check_query_results(graphql.client.query(
            DISCUSSION_RESOLVER_QUERY,
            variable_values={'namespace': namespace, 'mr_iid': mr_iid},
            paged_key='project/mergeRequest/discussions',
        ), check_keys={'project'})

        discussions = misc.get_nested_key(result, 'project/mergeRequest/discussions/nodes')
        count += len(discussions)
        if (discussion_list := [d for d in discussions if d['resolved']]):
            mr_list.append({mr_id: discussion_list})

    LOGGER.info('Project %s found %s discussions, with %s being resolved.', namespace, count,
                len(mr_list))

    return mr_list


def get_user_permission(graphql, namespace, username):
    """Return the user access level to the given project, or NO_ACCESS if not a member."""
    query_params = {'namespace': namespace, 'username': username}

    result = graphql.check_query_results(graphql.client.query(USER_PERMISSION, query_params),
                                         check_keys={'project'})
    if (searched_users := misc.get_nested_key(result, 'project/projectMembers/nodes')):
        if access_level := next((misc.get_nested_key(node, 'accessLevel/integerValue')
                                 for node in searched_users
                                 if misc.get_nested_key(node, 'user/username') == username),
                                NO_ACCESS):
            return access_level
    LOGGER.error('Could not fetch %s permission to project %s.', username, namespace)
    return NO_ACCESS


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check for improperly resolved threads')

    # Global options
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    return parser.parse_args()


def add_comment_to_discussion(graphql, noteableid, discussionid, body):
    """Add comment to gitlab MR discussion and change it resolved state."""
    query_params = {"noteableID": noteableid, "discussionID": discussionid, "body": body}

    result = graphql.check_query_results(graphql.client.query(DISCUSSION_ACTION, query_params),
                                         check_keys={'createNote'})

    if (misc.get_nested_key(result, 'createNote/errors') != [] or
            misc.get_nested_key(result, 'discussionToggleResolve/errors') != []):
        LOGGER.error('Could not add comment to discussion, createNote: %s, '
                     'discussionToggleResolve: %s.',
                     misc.get_nested_key(result, 'createNote/errors'),
                     misc.get_nested_key(result, 'discussionToggleResolve/errors'))
        sys.exit(1)


def main():
    # pylint: disable=too-many-nested-blocks
    """Find MRs opened with resolved threads."""
    args = _get_parser_args()
    LOGGER.info('Finding MRs with resolved thread...')

    graphql = GitlabGraph()
    for project in args.projects:
        LOGGER.info("Validating Project %s", project)
        if not (merge_requests := get_mrs_with_discussions(graphql, project)):
            LOGGER.info('All MRs from %s with resolved discussions have been validated,'
                        ' nothing to do.', project)
            continue

        for merge_request in merge_requests:
            for mr_id, discussions in merge_request.items():
                for discussion in discussions:
                    if not (resolved_by := misc.get_nested_key(discussion, 'resolvedBy/username')):
                        LOGGER.error('Could not get resolved_by for %s discussion: %s', mr_id,
                                     discussion)
                        continue

                    # Check if user is somehow involved with the thread or if the bot already
                    # commented on the thread
                    if all(misc.get_nested_key(author, 'author/username')
                           not in (resolved_by, *defs.BOT_ACCOUNTS)
                           for author in misc.get_nested_key(discussion, 'notes/nodes')):
                        # Check if the user who resolved the thread is maintainer or higher
                        if get_user_permission(graphql, project, resolved_by) < MAINTAINER_ACCESS:
                            author = misc.get_nested_key(
                                discussion, 'notes/nodes/0/author/username')
                            # Un-resolve discussion and notify author
                            if misc.is_production():
                                add_comment_to_discussion(graphql, mr_id, discussion['id'],
                                                          improperly_resolved_msg(resolved_by,
                                                                                  author))
                            LOGGER.info("Improperly resolved thread, user: %s thread: %s",
                                        misc.get_nested_key(discussion, 'resolvedBy/username'),
                                        misc.get_nested_key(discussion, 'notes/nodes/0/url'))
                    else:
                        LOGGER.info("User %s Match author or involved with discussion %s",
                                    misc.get_nested_key(discussion, 'resolvedBy/username'),
                                    misc.get_nested_key(discussion, 'notes/nodes/0/url'))


if __name__ == '__main__':
    main()
