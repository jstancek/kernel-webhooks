"""Tests for graphql module."""
import unittest
from unittest import mock

from gql.transport.exceptions import TransportQueryError

from webhook import graphql


class TestHelpers(unittest.TestCase):
    """Test helper functions."""

    @mock.patch('webhook.graphql.GitlabGraph._check_user', wraps=graphql.GitlabGraph._check_user)
    @mock.patch('webhook.graphql.GitlabGraph._check_keys', wraps=graphql.GitlabGraph._check_keys)
    def test_check_query_results(self, mock_check_keys, mock_check_user):
        """Test check_query_results."""
        # nothing to do
        mock_results = {}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, None, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_not_called()

        # check user and it doesn´t match
        mock_results = {'currentUser': {'username': 'cool_guy'}}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, None, 'steve') is mock_results)
        mock_check_user.assert_called_with(mock_results, 'steve')
        mock_check_keys.assert_not_called()

        # check user matches
        mock_check_user.reset_mock()
        self.assertEqual(graphql.GitlabGraph.check_query_results(
            mock_results, None, 'cool_guy'), None)
        mock_check_user.assert_called_with(mock_results, 'cool_guy')
        mock_check_keys.assert_not_called()

        # check keys and they are all there
        mock_check_user.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        self.assertTrue(graphql.GitlabGraph.check_query_results(
            mock_results, {'users'}, None) is mock_results)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'users'})

        # check keys and they are not all there
        mock_check_keys.reset_mock()
        mock_results = {'users': {}, 'groups': {}}
        with self.assertRaises(RuntimeError):
            graphql.GitlabGraph.check_query_results(mock_results, {'fans', 'users'}, None)
        mock_check_user.assert_not_called()
        mock_check_keys.assert_called_with(mock_results, {'fans', 'users'})


class TestGitlabGraph(unittest.TestCase):
    """Test GitlabGraph methods."""

    @mock.patch('cki_lib.gitlab._GitLabClient')
    def test_init(self, mock_client):
        """Test a new object sets up the client."""
        mygraph = graphql.GitlabGraph()
        self.assertEqual(mygraph.client, mock_client())

    @mock.patch('cki_lib.gitlab._GitLabClient')
    def test_user(self, mock_client):
        """Test the user* properties."""
        user_result = {'currentUser': {'id': '//gitlab/User/1234',
                                       'name': 'Example User',
                                       'username': 'user1'}
                       }
        mock_client.return_value.query.return_value = user_result
        mygraph = graphql.GitlabGraph(get_user=True)
        mygraph.client.query.assert_called_with(graphql.GET_USER_DETAILS_QUERY)
        self.assertEqual(mygraph.user, user_result['currentUser'])
        self.assertEqual(mygraph.username, user_result['currentUser']['username'])
        self.assertEqual(mygraph.user_id, 1234)

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_client_query(self):
        """Test the client.query method."""
        query = '{currentUser {username}}'
        mygraph = graphql.GitlabGraph()
        result = mygraph.client.query(query)
        mygraph.client.query.assert_called_with(query)
        self.assertEqual(result, mygraph.client.query.return_value)

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_client_query_error(self):
        """Test client.query raises the expected error."""
        query = '{currentUser1 {username}}'
        mygraph = graphql.GitlabGraph()
        mygraph.client.query.side_effect = \
            TransportQueryError("Encountered 1 error(s) executing query: {currentUser1 {username}}",
                                errors=["Field 'currentUser1' doesn't exist on type 'Query'"])

        with self.assertRaises(TransportQueryError):
            mygraph.client.query(query)

    def test_paged_query(self):
        """Test paged query returns the expected results."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.execute = mock.Mock()

        query = '{currentUser {username}}'
        paged_key = 'project/mergeRequest/commits'
        variable_values = {'namespace': 'group/project', 'mr_id': 123}

        # No results returns None
        mygraph.client.execute.return_value = None
        result = mygraph.client.query(query, variable_values=variable_values, paged_key=paged_key)
        self.assertEqual(result, None)
        mygraph.client.execute.assert_called_once_with(mock.ANY, variable_values=variable_values)

        # Some results
        mygraph.client.execute.reset_mock(return_value=True)

        # Expected results of client.query
        commits1 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Abc'}, 'nodes': [1, 2, 3]}
        commits2 = {'pageInfo': {'hasNextPage': True, 'endCursor': 'Def'}, 'nodes': [4, 5, 6]}
        commits3 = {'pageInfo': {'hasNextPage': False, 'endCursor': 'Ghi'}, 'nodes': [7, 8]}
        result1 = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        result2 = {'project': {'mergeRequest': {'commits': commits2}}}
        result3 = {'project': {'mergeRequest': {'commits': commits3}}}
        mygraph.client.execute.side_effect = [result1, result2, result3]

        result = mygraph.client.query(query, paged_key=paged_key, variable_values=variable_values)
        # Expected return of execute_paged_query is the first result updated with all node values
        commits1['pageInfo']['nodes'] = [1, 2, 3, 4, 5, 6, 7, 8]
        expected = {'project': {'mergeRequest': {'commits': commits1, 'desciption': 'hey'}}}
        self.assertEqual(result, expected)

        # client.query should be called thrice with updated cursor
        self.assertEqual(mygraph.client.execute.call_count, 3)
        call1 = mock.call(mock.ANY, variable_values=variable_values)
        variable_values['after'] = 'Abc'
        variable_values['first'] = False
        call2 = mock.call(mock.ANY, variable_values=variable_values)
        variable_values['after'] = 'Def'
        call3 = mock.call(mock.ANY, variable_values=variable_values)
        mygraph.client.execute.assert_has_calls([call1, call2, call3])

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_do_note(self):
        """Test do_note."""
        mygraph = graphql.GitlabGraph()
        mygraph.client.query = mock.Mock()

        # Production create.
        query_result = {'createNote': {'note': {'id': 456}}}
        mygraph.client.query.return_value = query_result
        with mock.patch('webhook.graphql.is_production', return_value=True):
            result = mygraph._do_note('create', 123, 'hello')
            self.assertIs(result, query_result)

        # Production create with no result.
        mygraph.client.query.return_value = None
        with mock.patch('webhook.graphql.is_production', return_value=True):
            with self.assertRaises(Exception):
                mygraph._do_note('create', 123, 'hello')

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_create_note(self):
        """Test create_note."""
        mygraph = graphql.GitlabGraph()
        mygraph._do_note = mock.Mock()

        # Not production.
        with mock.patch('webhook.graphql.is_production', return_value=False):
            result = mygraph.create_note(123, 'hello')
            self.assertIs(result, True)
            mygraph._do_note.assert_not_called()

        # Production.
        mygraph._do_note.return_value = {'createNote': {'note': {'id': 456}}}
        with mock.patch('webhook.graphql.is_production', return_value=True):
            result = mygraph.create_note(123, 'hello')
            self.assertEqual(result, 456)
            mygraph._do_note.assert_called_once_with('create', 123, 'hello')

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_update_note(self):
        """Test update_note."""
        mygraph = graphql.GitlabGraph()
        mygraph._do_note = mock.Mock()

        # Not production.
        with mock.patch('webhook.graphql.is_production', return_value=False):
            result = mygraph.update_note(123, 'hello')
            self.assertIs(result, True)
            mygraph._do_note.assert_not_called()

        # Production.
        mygraph._do_note.return_value = {'updateNote': {'note': {'id': 456}}}
        with mock.patch('webhook.graphql.is_production', return_value=True):
            result = mygraph.update_note(123, 'hello')
            self.assertEqual(result, 456)
            mygraph._do_note.assert_called_once_with('update', 123, 'hello')

    @mock.patch('cki_lib.gitlab._GitLabClient', mock.Mock())
    def test_replace_note(self):
        """Test replace_note."""
        mygraph = graphql.GitlabGraph()
        mygraph.create_note = mock.Mock()
        mygraph.update_note = mock.Mock()
        mygraph.client.query = mock.Mock()

        namespace = 'group/project'
        mr_id = 10
        username = 'mock_user'
        substring = '**Important Message**'
        body = 'new message'

        # No results.
        mygraph.client.query.return_value = None
        with self.assertRaises(Exception):
            mygraph.replace_note(namespace, mr_id, username, substring, body)

        # Found an existing note
        mygraph.client.query.reset_mock()
        note1 = {'author': {'username': 'steve'}, 'body': 'hey there', 'id': 'n1'}
        note2 = {'author': {'username': username}, 'body': f'hello {substring} there', 'id': 'n2'}
        note3 = {'author': {'username': 'bob'}, 'body': 'hi steve', 'id': 'n3'}
        discussions = [{'notes': {'nodes': [note1, note2, note3]}}]
        query_result = {'project': {'mergeRequest': {'discussions': {'nodes': discussions},
                                                     'id': 123}}}
        mygraph.client.query.return_value = query_result
        result = mygraph.replace_note(namespace, mr_id, username, substring, body)
        self.assertIs(result, mygraph.update_note.return_value)
        mygraph.update_note.assert_called_once_with('n2', body)
        mygraph.create_note.assert_not_called()

        # No existing note.
        mygraph.client.query.reset_mock()
        mygraph.create_note.reset_mock()
        mygraph.update_note.reset_mock()
        discussions = [{'notes': {'nodes': [note1, note3]}}]
        query_result = {'project': {'mergeRequest': {'discussions': {'nodes': discussions},
                                                     'id': 123}}}
        mygraph.client.query.return_value = query_result
        result = mygraph.replace_note(namespace, mr_id, username, substring, body)
        self.assertIs(result, mygraph.create_note.return_value)
        mygraph.create_note.assert_called_once_with(123, body)
        mygraph.update_note.assert_not_called()
